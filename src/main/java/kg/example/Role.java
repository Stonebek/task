

package kg.example;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by Mickey on 6/28/16.
 */
@Entity
@Table(name="role")
public class Role implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name", nullable = false, length = 80)
    private String name;

    @ManyToMany(cascade=CascadeType.ALL, mappedBy="role")
    private Set<User> user;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<User> getUser() {
        return user;
    }

    public void setUser(Set<User> user) {
        this.user = user;
    }
}
