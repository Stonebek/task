package kg.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Created by Mickey on 6/29/16.
 */
@Controller
public class UserController {

    @Autowired
    private UserServiceImplementation userServiceImplementation;

    @RequestMapping("/create")
    public String create() {

        User user = new User();
        user.setDescription("hahahahahahhahhah this is a desecription");
        user.setEmail("mykyky@gmail.com");
        user.setCreateDate(new Date());
        user.setEnabled(true);
        user.setFirstName("Myktybek");
        user.setLastName("Stone");

        userServiceImplementation.saveUser(user);
        return "user_is_saved";
    }
}
