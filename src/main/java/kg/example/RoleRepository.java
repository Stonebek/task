package kg.example;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mickey on 6/29/16.
 */
public interface RoleRepository extends JpaRepository<Role, Integer> {
}
