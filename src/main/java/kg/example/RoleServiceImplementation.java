package kg.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Mickey on 6/29/16.
 */
@Service
public class RoleServiceImplementation {

    @Autowired
    private RoleRepository roleRepository;

    @Transactional
    public Role saveRole(Role role){
        return roleRepository.save(role);
    }

    public void deleteRole(Role role){
        roleRepository.delete(role);
    }

    public List<Role> listAll() {
        return roleRepository.findAll();
    }

}
