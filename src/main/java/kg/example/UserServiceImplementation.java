package kg.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Mickey on 6/29/16.
 */
@Service
public  class UserServiceImplementation{
    @Autowired
    private UserRepository userRepository;


    public User getByUserById(Integer id) {
        return userRepository.findOne(id);
    }

    public List<User> listAll() {
        return userRepository.findAll();
    }

    @Transactional
    public void deleteUser(User user) {
        userRepository.delete(user);
    }

    @Transactional
    public User saveUser(User user) {
        return userRepository.save(user);
    }
}
